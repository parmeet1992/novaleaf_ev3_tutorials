from ev3dev.ev3 import *
import time



def move_tank(m1,m2,power1,power2,rotations=None):
        if rotations==None:
                m1.run_timed(speed_sp=power1)
                m2.run_timed(speed_sp=power2)
        else:
                m1.run_timed(time_sp=rotations,speed_sp=power1)
                m2.run_timed(time_sp=rotations,speed_sp=power2)
                                
if __name__ == '__main__':        
        ##drive forward in straight line at desired speed
        ts = TouchSensor() 
        
        m1 = LargeMotor('outB')
        m2 = LargeMotor('outC')
        
        move_tank(m1,m2,75,75,100)        
        while m1.state=='running' or m2.state=='running':
                if not ts.value():
                        m1.stop()
                        m2.stop()        
        ##wait for one second
        sleep(1)
        Leds.set_color(Leds.LEFT, Leds.AMBER)
        Leds.set_color(Leds.RIGHT, Leds.AMBER)
        
        move_tank(m1,m2,-75,-75,3)
        
        m1.wait_while('RUNNING')
        m2.wait_while('RUNNING')
        
        Leds.set_color(Leds.LEFT, Leds.GREEN)
        Leds.set_color(Leds.RIGHT, Leds.GREEN)
