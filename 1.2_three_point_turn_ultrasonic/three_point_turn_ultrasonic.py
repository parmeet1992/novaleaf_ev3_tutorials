from ev3dev.ev3 import *
import time



def move_tank_wait(port1,port2,power1,power2,rotations):
        m1 = LargeMotor(port1)
        m2 = LargeMotor(port2)
        m1.run_timed(time_sp=rotations,speed_sp=power1)
        m2.run_timed(time_sp=rotations,speed_sp=power2)
        m1.wait_while('running')
        m2.wait_while('running')
def move_tank(port1,port2,power1,power2,rotations):
        m1 = LargeMotor(port1)
        m2 = LargeMotor(port2)
        m1.run_timed(time_sp=rotations,speed_sp=power1)
        m2.run_timed(time_sp=rotations,speed_sp=power2)
        
if __name__ == '__main__':
        ##turn to right and stop after 1.5 seconds
        move_tank('outB','outC',75,30,1.5)
        us = UltrasonicSensor() 
        # Put the US sensor into distance mode.
        us.mode='US-DIST-CM'

        units = us.units
        ##reverse to left and stop after 1 second
        
        while(1):
                distance = us.value()/10
                if distance<7:
                        break
                move_tank('outB','outC',-30,-75,1.5)
        ##You should now be straight and facing the other way drive off
        move_tank('outB','outC',50,50,3)
